!classDefinition: #OOStackTest category: 'Stack-Exercise'!
TestCase subclass: #OOStackTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:30'!
test01StackShouldBeEmptyWhenCreated

	| stack |
	
	stack := OOStack new.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:29:55'!
test02PushAddElementsToTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	
	self deny: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:01'!
test03PopRemovesElementsFromTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:09'!
test04PopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	stack push: pushedObject.
	
	self assert: stack pop = pushedObject! !

!OOStackTest methodsFor: 'test' stamp: 'NR 9/16/2021 17:40:17'!
test05StackBehavesLIFO

	| stack firstPushedObject secondPushedObject |
	
	stack := OOStack new.
	firstPushedObject := 'firstSomething'.
	secondPushedObject := 'secondSomething'.
	
	stack push: firstPushedObject.
	stack push: secondPushedObject.
	
	self assert: stack pop = secondPushedObject.
	self assert: stack pop = firstPushedObject.
	self assert: stack isEmpty 
	! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:20'!
test06TopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack top = pushedObject.
	! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:24'!
test07TopDoesNotRemoveObjectFromStack

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack size = 1.
	stack top.
	self assert: stack size = 1.
	! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:26'!
test08CanNotPopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:31'!
test09CanNotPopWhenThereAreNoObjectsInTheStackAndTheStackHadObjects

	| stack  |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:44'!
test10CanNotTopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack top ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !


!classDefinition: #SentenceFinderByPrefixTest category: 'Stack-Exercise'!
TestCase subclass: #SentenceFinderByPrefixTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'mc 9/17/2023 18:31:15'!
does: sentenceCollection containSentence: sentence
	| sentenceIndex |
	
	sentenceIndex := (sentenceCollection indexOf: sentence ifAbsent: 0).
	^ sentenceIndex ~= 0.
	! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'mc 9/17/2023 17:37:40'!
test01SentencesCanBeAdded
	| sentenceFinder |

	sentenceFinder := SentenceFinderByPrefix new.
	sentenceFinder addSentence: 'Testing sentence1'.
	sentenceFinder addSentence: 'Testing sentence2'.
	sentenceFinder addSentence: 'Testing sentence3'.! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'mc 9/17/2023 17:40:47'!
test02EmptyPrefixShouldReturnError
	| sentenceFinder |

	sentenceFinder := SentenceFinderByPrefix new.
	
	self
		should: [ sentenceFinder findByPrefix: '']
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = SentenceFinderByPrefix invalidPrefixError ]
		! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'mc 9/17/2023 17:41:59'!
test03AllWhitespacePrefixShouldReturnError
	| sentenceFinder |

	sentenceFinder := SentenceFinderByPrefix new.
	
	self
		should: [ sentenceFinder findByPrefix: '    ']
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = SentenceFinderByPrefix invalidPrefixError ]
		! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'mc 9/17/2023 17:41:48'!
test04PrefixWithWhitespaceShouldReturnError
	| sentenceFinder |

	sentenceFinder := SentenceFinderByPrefix new.
	
	self
		should: [ sentenceFinder findByPrefix: 'test   ']
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = SentenceFinderByPrefix invalidPrefixError ]
		! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'mc 9/17/2023 18:53:22'!
test05ValidPrefixShouldBeCaseSensitive
	| sentenceFinder foundSentences addedSentence1 addedSentence2 |

	sentenceFinder := SentenceFinderByPrefix new.
	
	addedSentence1 := sentenceFinder addSentence: 'Testing sentence1'.
	addedSentence2 := sentenceFinder addSentence: 'Testing sentence2'.

	foundSentences := sentenceFinder findByPrefix: 'testing'.
	
	self deny: (self does: foundSentences containSentence: addedSentence1).
	self deny: (self does: foundSentences containSentence: addedSentence2).

	! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'mc 9/17/2023 18:53:28'!
test06ValidPrefixShouldFindSentences
	| sentenceFinder foundSentences addedSentence1 addedSentence2 |

	sentenceFinder := SentenceFinderByPrefix new.
	
	addedSentence1 := sentenceFinder addSentence: 'Testing sentence1'.
	addedSentence2 := sentenceFinder addSentence: 'Testing sentence2'.

	foundSentences := sentenceFinder findByPrefix: 'Testing'.
	
	self assert: (self does: foundSentences containSentence: addedSentence1).
	self assert: (self does: foundSentences containSentence: addedSentence2).

	! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'mc 9/17/2023 18:54:40'!
test07ShouldFindSentencesTwoTimes
	| sentenceFinder foundSentences addedSentence1 addedSentence2 |

	sentenceFinder := SentenceFinderByPrefix new.
	
	addedSentence1 := sentenceFinder addSentence: 'Testing sentence1'.
	addedSentence2 := sentenceFinder addSentence: 'Testing sentence2'.

	foundSentences := sentenceFinder findByPrefix: 'Testing'.
	
	self assert: (self does: foundSentences containSentence: addedSentence1).
	self assert: (self does: foundSentences containSentence: addedSentence2).
	
	foundSentences := sentenceFinder findByPrefix: 'Testing'.
	
	self assert: (self does: foundSentences containSentence: addedSentence1).
	self assert: (self does: foundSentences containSentence: addedSentence2).

	! !

!SentenceFinderByPrefixTest methodsFor: 'test' stamp: 'mc 9/17/2023 18:56:26'!
test08ShouldOnlyReturnCorrectSentences
	| sentenceFinder foundSentences addedSentence1 addedSentence2|

	sentenceFinder := SentenceFinderByPrefix new.
	
	addedSentence1 := sentenceFinder addSentence: 'Testing sentence1'.
	addedSentence2 := sentenceFinder addSentence: 'This one does not match'.

	foundSentences := sentenceFinder findByPrefix: 'Testing'.
	
	self assert: (self does: foundSentences containSentence: addedSentence1).
	self deny: (self does: foundSentences containSentence: addedSentence2).
	! !


!classDefinition: #OOStack category: 'Stack-Exercise'!
Object subclass: #OOStack
	instanceVariableNames: 'elementOnTop'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStack methodsFor: 'initialization' stamp: 'mc 9/17/2023 16:02:04'!
initialize
	elementOnTop := EmptyStackElement new.! !


!OOStack methodsFor: 'operations' stamp: 'mc 9/17/2023 15:58:39'!
isEmpty
	^elementOnTop isEmpty.! !

!OOStack methodsFor: 'operations' stamp: 'mc 9/17/2023 16:14:39'!
pop
	^elementOnTop popFrom: self.! !

!OOStack methodsFor: 'operations' stamp: 'mc 9/17/2023 16:02:39'!
push: contentToAdd
	elementOnTop  := (StackElement containing: contentToAdd over: elementOnTop) .
	
! !

!OOStack methodsFor: 'operations' stamp: 'mc 9/17/2023 15:47:27'!
size
	^elementOnTop size.! !

!OOStack methodsFor: 'operations' stamp: 'mc 9/17/2023 15:47:24'!
top
	^elementOnTop content.! !


!OOStack methodsFor: 'internal' stamp: 'mc 9/17/2023 15:57:33'!
popFilledStackElementOnTop
	| poppedElement |
	
	poppedElement := elementOnTop.
	elementOnTop  := poppedElement prevElement.
	^poppedElement content.! !

!OOStack methodsFor: 'internal' stamp: 'mc 9/17/2023 16:10:07'!
printOn: aStream

	aStream 
		print: elementOnTop! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'OOStack class' category: 'Stack-Exercise'!
OOStack class
	instanceVariableNames: ''!

!OOStack class methodsFor: 'error descriptions' stamp: 'NR 9/14/2023 08:12:21'!
stackEmptyErrorDescription
	
	^ 'stack is empty!!!!!!'! !


!classDefinition: #Sentence category: 'Stack-Exercise'!
Object subclass: #Sentence
	instanceVariableNames: 'sentenceString'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!Sentence methodsFor: 'internal' stamp: 'mc 9/17/2023 17:17:14'!
hasSubstring: substring inPosition: positionToLookAt
         ^(sentenceString findString: substring startingAt: 1) = positionToLookAt.
! !

!Sentence methodsFor: 'internal' stamp: 'mc 9/17/2023 17:55:51'!
printOn: aStream

	aStream 
		print: 'aSentence: ';
		print: sentenceString.! !

!Sentence methodsFor: 'internal' stamp: 'mc 9/17/2023 18:28:44'!
value
	^sentenceString.! !


!Sentence methodsFor: 'initialization' stamp: 'mc 9/17/2023 16:32:01'!
from: string
	sentenceString := string.! !


!Sentence methodsFor: 'operations' stamp: 'mc 9/17/2023 17:18:54'!
hasPrefix: prefix
	^self hasSubstring: prefix inPosition: 1.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Sentence class' category: 'Stack-Exercise'!
Sentence class
	instanceVariableNames: ''!

!Sentence class methodsFor: 'as yet unclassified' stamp: 'mc 9/17/2023 18:19:52'!
from: sentenceString
	^self new from: sentenceString! !


!classDefinition: #SentenceFinderByPrefix category: 'Stack-Exercise'!
Object subclass: #SentenceFinderByPrefix
	instanceVariableNames: 'sentenceStack'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefix methodsFor: 'initialization' stamp: 'mc 9/17/2023 17:21:07'!
initialize
	sentenceStack := OOStack new.! !


!SentenceFinderByPrefix methodsFor: 'operations' stamp: 'mc 9/17/2023 18:52:54'!
addSentence: stringToBeAdded
	sentenceStack push: (Sentence from: stringToBeAdded).
	^ stringToBeAdded .! !

!SentenceFinderByPrefix methodsFor: 'operations' stamp: 'mc 9/17/2023 18:49:51'!
findByPrefix: prefix
	(self isValidPrefix: prefix) ifFalse: [^self error: self class invalidPrefixError].
	
	^self select: [:sentence | sentence hasPrefix: prefix].! !


!SentenceFinderByPrefix methodsFor: 'internal' stamp: 'mc 9/17/2023 18:49:26'!
isValidPrefix: prefixToValidate
	(prefixToValidate isEmpty) ifTrue: [^false].
	(prefixToValidate withBlanksTrimmed size  ~= prefixToValidate size) ifTrue: [^false].
	
	^true.
	! !

!SentenceFinderByPrefix methodsFor: 'internal' stamp: 'mc 9/17/2023 18:24:06'!
iterateOnStack: stackToIterate doing: aClosureToEvaluateForEachElement

	[stackToIterate isEmpty] whileFalse: [
		aClosureToEvaluateForEachElement value: stackToIterate pop.
	].
	! !

!SentenceFinderByPrefix methodsFor: 'internal' stamp: 'mc 9/17/2023 17:32:15'!
iterateOnStackWithoutModifyingOrder: aClosureToEvaluateForEachElement
	| poppedElementsStack |
	
	poppedElementsStack := OOStack new.
	
	self iterateOnStack: sentenceStack doing: [:poppedElement | 
		aClosureToEvaluateForEachElement value: poppedElement.
		poppedElementsStack push: (poppedElement).	
	].

	self iterateOnStack: poppedElementsStack doing: [:poppedElement | 
		sentenceStack push: poppedElement.	
	].

	! !

!SentenceFinderByPrefix methodsFor: 'internal' stamp: 'mc 9/17/2023 18:37:43'!
select: conditionToCheck
	| foundElements |
	
	foundElements := OrderedCollection new.
	
	self iterateOnStackWithoutModifyingOrder: [:sentence | 
		(conditionToCheck value: sentence)	 ifTrue: [foundElements add: sentence value]
	].

	^foundElements.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'SentenceFinderByPrefix class' category: 'Stack-Exercise'!
SentenceFinderByPrefix class
	instanceVariableNames: ''!

!SentenceFinderByPrefix class methodsFor: 'error' stamp: 'mc 9/17/2023 17:41:04'!
invalidPrefixError
	^'Invalid prefix'! !


!classDefinition: #StackElement category: 'Stack-Exercise'!
Object subclass: #StackElement
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'StackElement class' category: 'Stack-Exercise'!
StackElement class
	instanceVariableNames: ''!

!StackElement class methodsFor: 'instanceCreation' stamp: 'mc 9/17/2023 16:05:08'!
containing: elementToContain over: previousStackElement
	^FilledStackElement new containing: elementToContain  over: previousStackElement .! !


!classDefinition: #EmptyStackElement category: 'Stack-Exercise'!
StackElement subclass: #EmptyStackElement
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!EmptyStackElement methodsFor: 'operators' stamp: 'mc 9/17/2023 15:58:49'!
isEmpty
	^ true.! !

!EmptyStackElement methodsFor: 'operators' stamp: 'mc 9/17/2023 16:16:08'!
popFrom: stack
	^self error: OOStack stackEmptyErrorDescription.! !

!EmptyStackElement methodsFor: 'operators' stamp: 'mc 9/17/2023 15:07:52'!
size
	^ 0.! !


!EmptyStackElement methodsFor: 'accesors' stamp: 'mc 9/17/2023 14:48:34'!
content
	^ self error: OOStack stackEmptyErrorDescription.! !

!EmptyStackElement methodsFor: 'accesors' stamp: 'mc 9/17/2023 14:53:21'!
prevElement
	^ self error: OOStack stackEmptyErrorDescription.! !


!classDefinition: #FilledStackElement category: 'Stack-Exercise'!
StackElement subclass: #FilledStackElement
	instanceVariableNames: 'content prevElement'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!FilledStackElement methodsFor: 'initialization' stamp: 'mc 9/17/2023 16:06:41'!
containing: elementToContain  over: previousStackElement .
	content := elementToContain.
	prevElement := previousStackElement.
! !


!FilledStackElement methodsFor: 'operators' stamp: 'mc 9/17/2023 15:58:59'!
isEmpty
	^false.! !

!FilledStackElement methodsFor: 'operators' stamp: 'mc 9/17/2023 15:46:53'!
popFrom: stack
	^stack popFilledStackElementOnTop.! !

!FilledStackElement methodsFor: 'operators' stamp: 'mc 9/17/2023 15:08:10'!
size
	^1 + (prevElement  size).! !


!FilledStackElement methodsFor: 'internal' stamp: 'mc 9/17/2023 16:11:38'!
printOn: aStream

	aStream 
		print: content;
		print: '->';
		print: prevElement! !


!FilledStackElement methodsFor: 'accesors' stamp: 'mc 9/17/2023 14:44:21'!
content
	^content! !

!FilledStackElement methodsFor: 'accesors' stamp: 'mc 9/17/2023 14:53:32'!
prevElement
	^prevElement! !
