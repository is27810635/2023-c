!classDefinition: #CantSuspend category: 'CodigoRepetido-Ejercicio'!
Error subclass: #CantSuspend
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #NotFound category: 'CodigoRepetido-Ejercicio'!
Error subclass: #NotFound
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!


!classDefinition: #CustomerBookTest category: 'CodigoRepetido-Ejercicio'!
TestCase subclass: #CustomerBookTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBookTest methodsFor: 'testing' stamp: 'mc 9/7/2023 13:24:15'!
executionOf: testToExecute shouldTakeNoMoreThan: maxTime
	| millisecondsBeforeRunning millisecondsAfterRunning |
	
	millisecondsBeforeRunning := Time millisecondClockValue * millisecond.
	testToExecute value.
	millisecondsAfterRunning := Time millisecondClockValue * millisecond.
	
	^ self assert: (millisecondsAfterRunning - millisecondsBeforeRunning < maxTime).
	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'mc 9/6/2023 23:19:43'!
in: customerBook thereAreSuspended: suspendedNumber thereAreActive: activeNumber 

	self assert: activeNumber equals: customerBook numberOfActiveCustomers.
	self assert: suspendedNumber equals: customerBook numberOfSuspendedCustomers.
	self assert: activeNumber + suspendedNumber equals: customerBook numberOfCustomers.
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'mc 9/7/2023 13:24:50'!
test01AddingCustomerShouldNotTakeMoreThan50Milliseconds

	| customerBook addCustomerTest |
	
	customerBook := CustomerBook new.
	addCustomerTest := [customerBook addCustomerNamed: 'John Lennon'.].
	
	self executionOf: addCustomerTest shouldTakeNoMoreThan: (50 * millisecond).
	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'mc 9/7/2023 13:25:13'!
test02RemovingCustomerShouldNotTakeMoreThan100Milliseconds

	| customerBook removeCustomerTest paulMcCartney |
	
	customerBook := CustomerBook new.
	paulMcCartney := 'Paul McCartney'.
	customerBook addCustomerNamed: paulMcCartney.
	removeCustomerTest := [customerBook removeCustomerNamed: paulMcCartney].

	self executionOf: removeCustomerTest shouldTakeNoMoreThan: (100 * millisecond).
	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'mc 9/7/2023 13:21:12'!
test03CanNotAddACustomerWithEmptyName 

	| customerBook |
			
	customerBook := CustomerBook new.

	self testAction: [customerBook addCustomerNamed: ''.]
	expectingError: Error
	thenAssert: [ :anError | 
			self assert: anError messageText = CustomerBook customerCanNotBeEmptyErrorMessage.
			self assert: customerBook isEmpty ]! !

!CustomerBookTest methodsFor: 'testing' stamp: 'mc 9/7/2023 13:19:59'!
test04CanNotRemoveAnInvalidCustomer
	
	| customerBook johnLennon |
			
	customerBook := CustomerBook new.
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.
	
	self testAction: [customerBook removeCustomerNamed: 'Paul McCartney'.]
	expectingError: NotFound
	thenAssert: [
		self assert: customerBook numberOfCustomers = 1.
		self assert: (customerBook includesCustomerNamed: johnLennon) 
	].
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'mc 9/6/2023 23:19:31'!
test05SuspendingACustomerShouldNotRemoveItFromCustomerBook

	| customerBook paulMcCartney|
	
	customerBook := CustomerBook new.
	paulMcCartney := 'Paul McCartney'.
	
	customerBook addCustomerNamed: paulMcCartney.
	customerBook suspendCustomerNamed: paulMcCartney.
	
	self in: customerBook thereAreSuspended: 1 thereAreActive: 0.
	self assert: (customerBook includesCustomerNamed: paulMcCartney).
	

	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'mc 9/6/2023 23:20:22'!
test06RemovingASuspendedCustomerShouldRemoveItFromCustomerBook

	| customerBook paulMcCartney|
	
	customerBook := CustomerBook new.
	paulMcCartney := 'Paul McCartney'.
	
	customerBook addCustomerNamed: paulMcCartney.
	customerBook suspendCustomerNamed: paulMcCartney.
	customerBook removeCustomerNamed: paulMcCartney.
	
	self in: customerBook thereAreSuspended: 0 thereAreActive: 0.
	self deny: (customerBook includesCustomerNamed: paulMcCartney).


	
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'mc 9/7/2023 13:19:13'!
test07CanNotSuspendAnInvalidCustomer
	
	| customerBook johnLennon |
			
	customerBook := CustomerBook new.
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.
	
	self testAction: [customerBook suspendCustomerNamed: 'George Harrison']
	expectingError: CantSuspend
	thenAssert: [self assert: (customerBook isTheOnlyCustomer: johnLennon)].
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'mc 9/7/2023 13:17:26'!
test08CanNotSuspendAnAlreadySuspendedCustomer
	
	| customerBook johnLennon |
			
	customerBook := CustomerBook new.
	johnLennon := 'John Lennon'.
	customerBook addCustomerNamed: johnLennon.
	customerBook suspendCustomerNamed: johnLennon.
	
	self testAction: [customerBook suspendCustomerNamed: johnLennon.]
	expectingError: CantSuspend
	thenAssert: [
		self assert: customerBook numberOfCustomers = 1.
		self assert: (customerBook includesCustomerNamed: johnLennon)
	].
! !

!CustomerBookTest methodsFor: 'testing' stamp: 'mc 9/7/2023 13:22:35'!
testAction: testToExecute expectingError: error thenAssert: anAssertion 

	[ testToExecute value. self fail]
		on: error 
		do: anAssertion	
! !


!classDefinition: #CustomerBook category: 'CodigoRepetido-Ejercicio'!
Object subclass: #CustomerBook
	instanceVariableNames: 'suspended active'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CodigoRepetido-Ejercicio'!

!CustomerBook methodsFor: 'testing' stamp: 'NR 4/3/2019 10:14:26'!
includesCustomerNamed: aName

	^(active includes: aName) or: [ suspended includes: aName ]! !

!CustomerBook methodsFor: 'testing' stamp: 'NR 4/3/2019 10:14:26'!
isEmpty
	
	^active isEmpty and: [ suspended isEmpty ]! !


!CustomerBook methodsFor: 'initialization' stamp: 'NR 9/17/2020 07:23:04'!
initialize

	active := OrderedCollection new.
	suspended:= OrderedCollection new.! !


!CustomerBook methodsFor: 'customer management' stamp: 'mc 9/7/2023 13:38:51'!
addCustomerNamed: aName

	aName isEmpty ifTrue: [ self signalCustomerNameCannotBeEmpty ].
	(self includesCustomerNamed: aName) ifTrue: [ self signalCustomerAlreadyExists ].
	
	active add: aName ! !

!CustomerBook methodsFor: 'customer management' stamp: 'mc 9/6/2023 23:01:23'!
isTheOnlyCustomer: name
	 
	^ self numberOfCustomers = 1 and: (self includesCustomerNamed: name)! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 4/3/2019 10:14:26'!
numberOfActiveCustomers
	
	^active size! !

!CustomerBook methodsFor: 'customer management' stamp: 'mc 9/6/2023 21:27:59'!
numberOfCustomers
	
	^self numberOfActiveCustomers + self numberOfSuspendedCustomers! !

!CustomerBook methodsFor: 'customer management' stamp: 'NR 9/19/2018 17:36:09'!
numberOfSuspendedCustomers
	
	^suspended size! !

!CustomerBook methodsFor: 'customer management' stamp: 'mc 9/7/2023 13:32:58'!
remove: anElement from: aCollection ifNotFound: notFoundAction  
 
	1 to: aCollection size do: 
	[ :index |
		(anElement = (aCollection at: index)) ifTrue: [aCollection removeAt: index. ^anElement]
	].

	^notFoundAction value.
! !

!CustomerBook methodsFor: 'customer management' stamp: 'mc 9/7/2023 13:34:38'!
removeCustomerNamed: aName 
 
	self remove: aName from: active ifNotFound: 
		[self remove: aName from: suspended ifNotFound: 
			[^ NotFound signal]
		]
! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:52'!
signalCustomerAlreadyExists 

	self error: self class customerAlreadyExistsErrorMessage! !

!CustomerBook methodsFor: 'customer management' stamp: 'HernanWilkinson 7/6/2011 17:51'!
signalCustomerNameCannotBeEmpty 

	self error: self class customerCanNotBeEmptyErrorMessage ! !

!CustomerBook methodsFor: 'customer management' stamp: 'mc 9/7/2023 13:36:37'!
suspendCustomerNamed: aName 
	
	self remove: aName from: active ifNotFound: [^CantSuspend signal].
	
	suspended add: aName
! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerBook class' category: 'CodigoRepetido-Ejercicio'!
CustomerBook class
	instanceVariableNames: ''!

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 9/4/2023 17:02:48'!
customerAlreadyExistsErrorMessage

	^'Customer already exists!!!!!!'! !

!CustomerBook class methodsFor: 'error messages' stamp: 'NR 9/4/2023 17:02:52'!
customerCanNotBeEmptyErrorMessage

	^'Customer name cannot be empty!!!!!!'! !
